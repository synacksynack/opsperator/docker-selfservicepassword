#!/bin/sh

set -e
if test "$DEBUG"; then
    DEBUG_SSP=true
    set -x
else
    DEBUG_SSP=false
fi
. /usr/local/bin/nsswrapper.sh

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
AUTH_METHOD=${AUTH_METHOD:-none}
BACKGROUND_IMAGE=${BACKGROUND_IMAGE:-wall_dark.jpg}
CLIENT_IP_HEADER=${CLIENT_IP_HEADER:-X-Forwarded-For}
HASH_METHOD=${HASH_METHOD:-clear}
LANG=${LANG:-en}
LOGO_IMAGE=${LOGO_IMAGE:-kubelemon.png}
NOTIFYONCHANGE=${NOTIFYONCHANGE:-false}
OIDC_CALLBACK_URL=${OIDC_CALLBACK_URL:-/oauth2/callback}
OIDC_CLIENT_ID=${OIDC_CLIENT_ID:-changeme}
OIDC_CLIENT_SECRET=${OIDC_CLIENT_SECRET:-secret}
OIDC_CRYPTO_SECRET=${OIDC_CRYPTO_SECRET:-secret}
OIDC_META_URL=${OIDC_META_URL:-/.well-known/openid-configuration}
OIDC_TOKEN_ENDPOINT_AUTH=${OIDC_TOKEN_ENDPOINT_AUTH:-client_secret_basic}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=ssp,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_CONF_DN_PREFIX="${OPENLDAP_CONF_DN_PREFIX:-ou=lemonldap,ou=config}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-127.0.0.1}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
RATELIMIT_BLOCK=${RATELIMIT_BLOCK:-60}
RATELIMIT_MAX_PER_IP=${RATELIMIT_MAX_PER_IP:-2}
RATELIMIT_MAX_PER_USER=${RATELIMIT_MAX_PER_USER:-2}
SMSATTR=${SMSATTR:-phone}
SMSCCPFX=${SMSCCPFX:-}
SMSMETHOD=${SMSMETHOD:-mail}
USEQUESTION=${USEQUESTION:-false}
USERATELIMIT=${USERATELIMIT:-false}
USEREST=${USEREST:-false}
USESMS=${USESMS:-false}
USESSH=${USESSH:-false}
USETOKENS=${USETOKENS:-false}
SMTP_AUTOTLS=${SMTP_AUTOTLS:-false}
SMTP_HOST=${SMTP_HOST:-}
SMTP_PASS=${SMTP_PASS:-}
SMTP_PORT=${SMTP_PORT:-25}
SMTP_SECURE=${SMTP_SECURE:-}
SMTP_USER=${SMTP_USER:-}
WHOCHANGESPW=${WHOCHANGESPW:-user}
if test -z "$CRYPT_KEY"; then
    CRYPT_KEY=$(cat /dev/urandom | head | sha1sum | cut -d " " -f 1)
fi
if test "$SMTP_USER" -a "$SMTP_PASS"; then
    SMTP_AUTH=true
else
    SMTP_AUTH=false
fi
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$APACHE_DOMAIN"; then
    APACHE_DOMAIN=password.$OPENLDAP_DOMAIN
fi
if test "$AUTH_METHOD" = oidc -a -z "$OIDC_PORTAL"; then
    OIDC_PORTAL=$PUBLIC_PROTO://auth.$OPENLDAP_DOMAIN
fi
if test "$OPENLDAP_PROTO" = ldaps; then
    LDAP_START_TLS=false
elif test "$LDAP_START_TLS"; then
    LDAP_START_TLS=true
else
    LDAP_START_TLS=false
fi
if test -z "$SMSMAILRCPTFMT"; then
    SMSMAILRCPTFMT={sms_attribute}@email.smsglobal.com
fi
if test "$SMTP_SECURE" -o "$SMTP_PORT" = 465; then
    if ! echo "$SMTP_SECURE" | grep -E '^(ssl|tls)$' >/dev/null; then
	if test "$SMTP_PORT" = 465; then
	    SMTP_SECURE=ssl
	else
	    SMTP_SECURE=tls
	fi
    fi
    SMTP_AUTOTLS=false
fi
if ! test "$NOTIFYONCHANGE" = false; then
    if test "$SMTP_HOST"; then
	NOTIFYONCHANGE=true
    else
	NOTIFYONCHANGE=false
    fi
fi
if test -z "$WHOCHANGESSSHKEY"; then
    WHOCHANGESSSHKEY=$WHOCHANGESPW
fi
if ! test -s "/usr/share/self-service-password/htdocs/images/$BACKGROUND_IMAGE"; then
    BACKGROUND_IMAGE=wall_dark.jpg
fi
if ! test -s "/usr/share/self-service-password/htdocs/images/$LOGO_IMAGE"; then
    LOGO_IMAGE=kubelemon.png
fi
export APACHE_DOMAIN
export APACHE_HTTP_PORT
export OPENLDAP_BASE
export OPENLDAP_BIND_DN_PREFIX
export OPENLDAP_DOMAIN
export OPENLDAP_HOST
export PUBLIC_PROTO
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
export RESET_TLS=false

if ! test -s /usr/share/self-service-password/conf/config.inc.local.php; then
    echo "Install SelfServicePassword Site Configuration"
    sed -e "s|BACKGROUND_IMAGE|$BACKGROUND_IMAGE|" \
	-e "s|CLIENT_IP_HEADER|$CLIENT_IP_HEADER|" \
	-e "s|CRYPT_KEY|$CRYPT_KEY|" \
	-e "s|DEBUG_PLACEHOLDER|$DEBUG_SSP|" \
	-e "s|HASH_METHOD|$HASH_METHOD|" \
	-e "s|LDAP_BASE|$OPENLDAP_BASE|" \
	-e "s|LDAP_BIND_DN|$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE|" \
	-e "s|LDAP_BIND_PW|$OPENLDAP_BIND_PW|" \
	-e "s|LANG_PLACEHOLDER|$LANG|" \
	-e "s|LDAP_HOST|$OPENLDAP_HOST|" \
	-e "s|LDAP_SCHEME|$OPENLDAP_PROTO|" \
	-e "s|LDAP_START_TLS|$LDAP_START_TLS|" \
	-e "s|LDAP_PORT|$OPENLDAP_PORT|" \
	-e "s|LOGO_IMAGE|$LOGO_IMAGE|" \
	-e "s|NOTIFYONCHANGE|$NOTIFYONCHANGE|" \
	-e "s|RATELIMIT_BLOCK|$RATELIMIT_BLOCK|" \
	-e "s|RATELIMIT_MAX_PER_IP|$RATELIMIT_MAX_PER_IP|" \
	-e "s|RATELIMIT_MAX_PER_USER|$RATELIMIT_MAX_PER_USER|" \
	-e "s|SMS_ATTR|$SMSATTR|" \
	-e "s|SMS_COUNTRYCODE_PFX|$SMSCCPFX|" \
	-e "s|SMS_MAILTO|$SMSMAILRCPTFMT|" \
	-e "s|SMS_METHOD|$SMSMETHOD|" \
	-e "s|SMTP_AUTH|$SMTP_AUTH|" \
	-e "s|SMTP_AUTOTLS|$SMTP_AUTOTLS|" \
	-e "s|SMTP_HOST|$SMTP_HOST|" \
	-e "s|SMTP_PASS|$SMTP_PASS|" \
	-e "s|SMTP_PORT|$SMTP_PORT|" \
	-e "s|SMTP_SECURE|$SMTP_SECURE|" \
	-e "s|SMTP_USER|$SMTP_USER|" \
	-e "s|SMTP_MAIL_FROM|$SMTP_MAIL_FROM|" \
	-e "s|USE_QUESTION|$USEQUESTION|" \
	-e "s|USE_RATELIMIT|$USERATELIMIT|" \
	-e "s|USE_REST|$USEREST|" \
	-e "s|USE_SMS|$USESMS|" \
	-e "s|USE_SSH|$USESSH|" \
	-e "s|USE_TOKENS|$USETOKENS|" \
	-e "s|WHO_CHANGES_PW|$WHOCHANGESPW|" \
	-e "s|WHO_CHANGES_SSHKEY|$WHOCHANGESSSHKEY|" \
	/config.inc.php \
	>/usr/share/self-service-password/conf/config.inc.local.php
    chmod 0640 /usr/share/self-service-password/conf/config.inc.local.php
    if test "$NOTIFICATIONS_KIND" = slack -a "$NOTIFICATIONS_TOKEN"; then
	cat <<EOF >>/usr/share/self-service-password/conf/config.inc.local.php
\$use_httpreset = \$use_tokens;
\$http_notifications_address = 'https://slack.com/api/chat.postMessage';
\$http_notifications_body = array(
	"channel"  => "@{login}",
	"text"     => "{data}",
	"username" => "self-service-password"
    );
\$http_notifications_headers = array(
	"Authorization: Bearer $NOTIFICATIONS_TOKEN",
	"Content-Type: application/x-www-form-urlencoded"
    );
\$http_notifications_method = 'POST';
\$http_notifications_params = array();
EOF
    elif test "$ROCKET_ADDRESS" -a "$NOTIFICATIONS_KIND" = rocket -a "$NOTIFICATIONS_USER" -a "$NOTIFICATIONS_TOKEN"; then
	cat <<EOF >>/usr/share/self-service-password/conf/config.inc.local.php
\$use_httpreset = \$use_tokens;
\$http_notifications_address = '$ROCKET_ADDRESS/api/v1/chat.postMessage';
\$http_notifications_body = array(
	"alias"  => "self-service-password",
	"roomId" => "@{login}",
	"text"   => "{data}"
    );
\$http_notifications_headers = array(
	"Content-Type: application/json",
	"X-Auth-Token: $NOTIFICATIONS_TOKEN",
	"X-User-Id: $NOTIFICATIONS_USER"
    );
\$http_notifications_method = 'POST';
\$http_notifications_params = array();
EOF
    fi
fi
if ! ls /etc/apache2/sites-enabled/*.conf >/dev/null 2>&1; then
    echo "Generates SelfServicePassword VirtualHost Configuration"
    (
	if test "$AUTH_METHOD" = lemon; then
	    cat <<EOF
PerlOptions +GlobalRequest
PerlModule Lemonldap::NG::Handler::ApacheMP2
EOF
	fi
	cat <<EOF
<VirtualHost *:$APACHE_HTTP_PORT>
    ServerName $APACHE_DOMAIN
    CustomLog /dev/stdout modremoteip
    Include "/etc/apache2/$SSL_INCLUDE.conf"
    AddDefaultCharset UTF-8
    Alias /favicon.ico /usr/share/self-service-password/htdocs/images/favicon.ico
    DirectoryIndex index.php
    DocumentRoot /usr/share/self-service-password/htdocs
EOF
	if test "$AUTH_METHOD" = ldap; then
	    cat <<EOF
    <Directory /usr/share/self-service-password/htdocs>
	AuthType Basic
	AuthBasicProvider ldap
	AuthLDAPBindDN "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE
	AuthLDAPBindPassword "$OPENLDAP_BIND_PW"
	AuthLDAPURL "$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT/$OPENLDAP_SEARCH" NONE
	AuthName "LDAP Auth"
	AllowOverride None
	Require valid-user
    </Directory>
EOF
	elif test "$AUTH_METHOD" = oidc; then
	    if ! echo "$OIDC_CALLBACK_URL" | grep ^/ >/dev/null; then
		OIDC_CALLBACK_URL=/$OIDC_CALLBACK_URL
	    fi
	    CALLBACK_ROOT_SUB_COUNT=`echo $OIDC_CALLBACK_URL | awk -F/ '{print NF}'`
	    CALLBACK_ROOT_SUB_COUNT=`expr $CALLBACK_ROOT_SUB_COUNT - 2 2>/dev/null`
	    if ! test "$CALLBACK_ROOT_SUB_COUNT" -ge 2; then
		CALLBACK_ROOT_SUB_COUNT=2
	    fi
	    CALLBACK_ROOT_URL=`echo $OIDC_CALLBACK_URL | cut -d/ -f 2-$CALLBACK_ROOT_SUB_COUNT`
	    cat <<EOF
    OIDCClientID $OIDC_CLIENT_ID
    OIDCClientSecret $OIDC_CLIENT_SECRET
    OIDCCryptoPassphrase $OIDC_CRYPTO_SECRET
    OIDCProviderMetadataURL $OIDC_PORTAL$OIDC_META_URL
    OIDCProviderTokenEndpointAuth $OIDC_TOKEN_ENDPOINT_AUTH
    OIDCRedirectURI $PUBLIC_PROTO://$APACHE_DOMAIN$OIDC_CALLBACK_URL
    OIDCRemoteUserClaim sub
    RequestHeader set REMOTE_USER expr=%{REMOTE_USER}
    <Location />
	AuthType openid-connect
	Require valid-user
    </Location>
    <Location /$CALLBACK_ROOT_URL/>
	AuthType openid-connect
	Require valid-user
    </Location>
EOF
	    if test "$PING_PATH"; then
		if echo "$PING_PATH" | grep ^/ >/dev/null; then
		    PING_PATH=`echo $PING_PATH | sed 's|^/||'`
		fi
		if ! test -d "$PING_ROOT"; then
		    PING_ROOT=/var/www/html
		fi
		cat <<EOF
    Alias /$PING_PATH $PING_ROOT
    <Location /$PING_PATH/>
	DirectoryIndex index.html
	Require all granted
    </Location>
EOF
	    fi
	else
	    if test "$AUTH_METHOD" = lemon; then
		cat <<EOF
    PerlHeaderParserHandler Lemonldap::NG::Handler::ApacheMP2
EOF
	    fi
	    cat <<EOF
    <Directory /usr/share/self-service-password/htdocs>
	Require all granted
    </Directory>
EOF
	fi
	echo "</VirtualHost>"
    ) >/etc/apache2/sites-enabled/003-vhosts.conf
    if test "$AUTH_METHOD" = oidc; then
	export APACHE_IGNORE_OPENLDAP=yay
	if test "$PUBLIC_PROTO" = https -a "$OIDC_SKIP_TLS_VERIFY"; then
	    sed -i \
		-e 's|^#*[ ]*OIDCSSLValidateServer.*|OIDCSSLValidateServer Off|' \
		-e 's|^#*[ ]*OIDCValidateIssuer.*|OIDCValidateIssuer Off|' \
		/etc/apache2/mods-enabled/auth_openidc.conf
	else
	    sed -i \
		-e 's|^#*[ ]*OIDCSSLValidateServer.*|OIDCSSLValidateServer On|' \
		-e 's|^#*[ ]*OIDCValidateIssuer.*|OIDCValidateIssuer On|' \
		/etc/apache2/mods-enabled/auth_openidc.conf
	fi
	sed -i \
	    -e 's|^#*[ ]*OIDCPassClaimsAs.*|OIDCPassClaimsAs environment|' \
	    -e 's|^#*[ ]*OIDCProviderAuthRequestMethod.*|OIDCProviderAuthRequestMethod GET|' \
	    -e 's|^#*[ ]*OIDCUserInfoTokenMethod.*|OIDCUserInfoTokenMethod authz_header|' \
	    -e 's|^#*[ ]*OIDCSessionCookieChunkSize.*|OIDCSessionCookieChunkSize 4000|' \
	    -e 's|^#*[ ]*OIDCSessionType.*|OIDCSessionType client-cookie|' \
	    -e 's|^#*[ ]*OIDCStripCookies.*|OIDCStripCookies mod_auth_openidc_session mod_auth_openidc_session_chunks mod_auth_openidc_session_0 mod_auth_openidc_session_1|' \
	    /etc/apache2/mods-enabled/auth_openidc.conf
    elif test "$AUTH_METHOD" = ldap; then
	export APACHE_IGNORE_OPENLDAP=yay
	rm -f /etc/apache2/mods-enabled/*openid*
	if test "$OPENLDAP_PROTO" = ldaps -a "$LDAP_SKIP_TLS_VERIFY"; then
	    echo "LDAPVerifyServerCert Off" \
		>>/etc/apache2/sites-enabled/003-vhosts.conf
	fi
    else
	rm -f /etc/apache2/mods-enabled/*openid*
    fi
    if ! test -s /etc/ldap/ldap.conf; then
	if test "$OPENLDAP_PROTO" = ldaps -a "$LDAP_SKIP_TLS_VERIFY"; then
	    cat <<EOF
URI		$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT
TLS_REQCERT	never
EOF
	elif test "$OPENLDAP_PROTO" = ldaps; then
	    cat <<EOF
URI		$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT
TLS_REQCERT	demand
EOF
	fi >/etc/ldap/ldap.conf
    fi
fi

unset USEQUESTION WHOCHANGESPW LDAP_START_TLS LANG DEBUG_SSP WHOCHANGESSSHKEY \
    USESSH CRYPT_KEY NOTIFICATIONS_TOKEN NOTIFICATIONS_USER NOTIFICATIONS_KIND \
    ROCKET_ADDRESS CALLBACK_ROOT_SUB_COUNT OIDC_CALLBACK_URL OIDC_CLIENT_ID \
    PING_ROOT OIDC_CLIENT_SECRET OIDC_CRYPTO_SECRET OIDC_META_URL OIDC_PORTAL \
    OIDC_TOKEN_ENDPOINT_AUTH CALLBACK_ROOT_URL

. /run-apache.sh
