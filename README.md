# k8s SelfServicePassword

LTB SelfServicePassword image.

Based on https://ltb-project.org/documentation/self-service-password

Diverts from https://gitlab.com/synacksynack/opsperator/docker-php

Depends on an OpenLDAP server, such as
https://gitlab.com/synacksynack/opsperator/docker-openldap

Build with:

```
$ make build
```

Test with:

```
$ make run
```

Start Demo or Cluster in OpenShift:

```
$ make ocdemo
$ make ocprod
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name            |    Description                 | Default                                                      | Inherited From    |
| :-------------------------- | ------------------------------ | ------------------------------------------------------------ | ----------------- |
|  `APACHE_DOMAIN`            | SSP ServerName                 | `password.${OPENLDAP_DOMAIN}`                                | opsperator/apache |
|  `APACHE_HTTP_PORT`         | SSP HTTP(s) Port               | `8080`                                                       | opsperator/apache |
|  `AUTH_METHOD`              | SSP Auth Method                | `none`, could be `lemon`, `ldap`, `oidc` or `none`           | opsperator/apache |
|  `BACKGROUND_IMAGE`         | SSP Background Image           | `wall_dark.jpg` (should be in config/ building image)        |                   |
|  `CLIENT_IP_HEADER`         | RateLimit Client IP Header     | `X-Forwarded-For`                                            |                   |
|  `CRYPT_KEY`                | Token Encryption Key           | randmon, changing on each container restart                  |                   |
|  `HASH_METHOD`              | SSP Hash Method                | `auto`                                                       |                   |
|  `LANG`                     | SSP Language                   | `en`                                                         |                   |
|  `LDAP_START_TLS`           | OpenLDAP uses StartTLS         | false                                                        |                   |
|  `LOGO_IMAGE`               | SSP Logo Image                 | `kubelemon.png` (should be in config/ building image)        |                   |
|  `NOTIFYONCHANGE`           | Email Notification on Changes  | `true`, unless SMTP not configured                           |                   |
|  `NOTIFICATIONS_KIND`       | SSP HTTP Notifications Kind    | undef (`slack` or `rocket`)                                  |                   |
|  `NOTIFICATIONS_TOKEN`      | SSP Notifications Token        | undef                                                        |                   |
|  `NOTIFICATIONS_USER`       | SSP Notifications User         | undef (required for `rocket` notifications)                  |                   |
|  `OIDC_CALLBACK_URL`        | OpenID Callback Path           | `/oauth2/callback`                                           |                   |
|  `OIDC_CLIENT_ID`           | OpenID Client ID               | `changeme`                                                   |                   |
|  `OIDC_CLIENT_SECRET`       | OpenID Client Secret           | `secret`                                                     |                   |
|  `OIDC_CRYPTO_SECRET`       | OpenID Crypto Secret           | `secret`                                                     |                   |
|  `OIDC_META_URL`            | OpenID Meta Path               | `/.well-known/openid-configuration`                          |                   |
|  `OIDC_PORTAL`              | OpenID Portal                  | `https://auth.$OPENLDAP_DOMAIN`                              |                   |
|  `OIDC_SKIP_TLS_VERIFY`     | OpenID Skip TLS Verify         | undef                                                        |                   |
|  `OIDC_TOKEN_ENDPOINT_AUTH` | OpenID Auth Method             | `client_secret_basic`                                        |                   |
|  `ONLY_TRUST_KUBE_CA`       | Don't trust base image CAs     | `false`, any other value disables ca-certificates CAs        | opsperator/apache |
|  `OPENLDAP_BASE`            | OpenLDAP Base                  | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local`  | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX`  | OpenLDAP Bind DN Prefix        | `cn=ssp,ou=services`                                         | opsperator/apache |
|  `OPENLDAP_BIND_PW`         | OpenLDAP Bind Password         | `secret`                                                     | opsperator/apache |
|  `OPENLDAP_CONF_DN_RREFIX`  | OpenLDAP Conf DN Prefix        | `cn=lemonldap,ou=config`                                     | opsperator/apache |
|  `OPENLDAP_DOMAIN`          | OpenLDAP Domain Name           | `demo.local`                                                 | opsperator/apache |
|  `OPENLDAP_HOST`            | OpenLDAP Backend Address       | `127.0.0.1`                                                  | opsperator/apache |
|  `OPENLDAP_PORT`            | OpenLDAP Bind Port             | `389` or `636` depending on `OPENLDAP_PROTO`                 | opsperator/apache |
|  `OPENLDAP_PROTO`           | OpenLDAP Proto                 | `ldap`                                                       | opsperator/apache |
|  `PHP_ERRORS_LOG`           | PHP Errors Logs Output         | `/proc/self/fd/2`                                            | opsperator/php    |
|  `PHP_MAX_EXECUTION_TIME`   | PHP Max Execution Time         | `30` seconds                                                 | opsperator/php    |
|  `PHP_MAX_FILE_UPLOADS`     | PHP Max File Uploads           | `20`                                                         | opsperator/php    |
|  `PHP_MAX_POST_SIZE`        | PHP Max Post Size              | `8M`                                                         | opsperator/php    |
|  `PHP_MAX_UPLOAD_FILESIZE`  | PHP Max Upload File Size       | `2M`                                                         | opsperator/php    |
|  `PHP_MEMORY_LIMIT`         | PHP Memory Limit               | `-1` (no limitation)                                         | opsperator/php    |
|  `PUBLIC_PROTO`             | Apache Public Proto            | `http`                                                       | opsperator/apache |
|  `RATELIMIT_BLOCK`          | Ratelimit Block Time           | `60` (seconds)                                               |                   |
|  `RATELIMIT_MAX_PER_IP`     | Ratelimit Max Action per IP    | `2`                                                          |                   |
|  `RATELIMIT_MAX_PER_USER`   | Ratelimit Max Action per User  | `2`                                                          |                   |
|  `ROCKET_ADDRESS`           | Rocket.Chat Notifications URL  | undef                                                        |                   |
|  `SMSATTR`                  | LDAP Attribute sending SMS     | `phone` (1)                                                  |                   |
|  `SMSCCPFX`                 | SMS Country Code Prefix        | `` (1)                                                       |                   |
|  `SMSMAILRCPTFMT`           | SMS RCPT address (mail method) | `{sms_attribute}@email.smsglobal.com` (`$SMSCCPFX` prefixed) |                   |
|  `SMSMETHOD`                | SMS Method (mail or api)       | `mail` (1)                                                   |                   |
|  `SMTP_AUTOTLS`             | SMTP Relay prefers StartTLS    | `false`                                                      |                   |
|  `SMTP_HOST`                | SMTP Relay Host                | undef                                                        |                   |
|  `SMTP_PASS`                | SMTP Relay Auth Password       | undef                                                        |                   |
|  `SMTP_PORT`                | SMTP Relay Port                | `25`                                                         |                   |
|  `SMTP_SECURE`              | SMTP Relay Protocol            | undef - use `ssl` for TLS, and `tls` for starttls            |                   |
|  `SMTP_USER`                | SMTP Relay Auth Username       | undef                                                        |                   |
|  `USEQUESTION`              | Question-based password reset  | `false`                                                      |                   |
|  `USERATELIMIT`             | Enables SSP Rate Limit         | `false`                                                      |                   |
|  `USEREST`                  | Enables SSP Restful API        | `false`                                                      |                   |
|  `USESMS`                   | Configures SMS Passwords Reset | `false` (1)                                                  |                   |
|  `USESSH`                   | Allows SSH Public Key changes  | `false`                                                      |                   |
|  `USETOKENS`                | Mail-based password reset      | `false` (based on PHP sesions / won't scale)                 |                   |
|  `WHOCHANGESPW`             | Who Binds to LDAP (passwords)  | `user`                                                       |                   |
|  `WHOCHANGESSSHKEY`         | Who Binds to LDAP (ssh keys)   | `user`                                                       |                   |

Note (1), setting up SMS integration would involve some third party provider,
see: https://ltb-project.org/documentation/self-service-password/1.3/config_sms


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point                             | Description                     | Inherited From    |
| :---------------------------------------------- | ------------------------------- | ----------------- |
|  `/certs`                                       | Apache Certificate (optional)   | opsperator/apache |
|  `/var/cache/self-service-password/templates_c` | SSP Smarty Templates            |                   |
